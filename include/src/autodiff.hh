// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef AUTODIFF_HH
#define AUTODIFF_HH
// -- AUTOMATIC DIFFERENTIATION via eigen --------------------------------------
#include <Eigen/Core>
#include <eigen_autodiff.hh>
#include <iostream>
#include <string>
using namespace Eigen;
// Define a AD Function (AD_Decl_Funtion) with AD_Vector as argument
typedef AutoDiffScalar<
    Matrix<AutoDiffScalar<Matrix<double, Dynamic, 1>>, Dynamic, 1>>
    AD_Decl_Function;
typedef Matrix<AD_Decl_Function, Dynamic, 1> AD_Vector;
// Get the calue, gradient and hessian related to a vector x to a function
class AD_Vector_Class {
public:
  AD_Vector_Class(VectorXd vector) {
    const int size = vector.size();
    ad_vector.resize(size);
    // copy values from input
    for (int i = 0; i < size; ++i) {
      ad_vector(i).value().value() = vector(i);
    }
    for (int i = 0; i < size; ++i) {
      init_twice_active_var(ad_vector(i), size, i);
    }
  };
  operator AD_Vector() { return ad_vector; }

private:
  AD_Vector ad_vector;
  template <typename T> void init_twice_active_var(T &ad, int d_num, int idx) {
    ad.value().derivatives() = T::DerType::Scalar::DerType::Unit(d_num, idx);
    ad.derivatives() = T::DerType::Unit(d_num, idx);
    for (int idx = 0; idx < d_num; idx++) {
      ad.derivatives()(idx).derivatives() =
          T::DerType::Scalar::DerType::Zero(d_num);
    }
  }
};
// AD_Function has a value, gradient and hessian for a given vector x
class AD_Function {
public:
  AD_Function(AD_Decl_Function(function)(AD_Vector, VectorXd), VectorXd x,
              VectorXd gen) {
    x_size = x.size();
    AD_Vector_Class ad_vector(x);
    func = function(ad_vector, gen);
  };
  operator AD_Decl_Function() { return func; }
  double val() { return func.value().value(); }
  VectorXd grad() { return func.value().derivatives(); }
  MatrixXd hes() {
    MatrixXd hessian(x_size, x_size);
    for (int i = 0; i < x_size; ++i) {
      hessian.middleRows(i, 1) =
          func.derivatives()(i).derivatives().transpose();
    }
    return hessian;
  }

private:
  AD_Decl_Function func;
  int x_size;
};
// Extend definition of power function: x^y = x[0]^y[0] * x[1]^y[1] ...
namespace Eigen {
inline AD_Decl_Function pow(AD_Vector x, VectorXd y) {
  AD_Decl_Function ad_power = (AD_Decl_Function)1;
  for (int k = 0; k < y.size(); ++k) {
    ad_power *= pow(x(k), y(k));
  }
  return ad_power;
}
} // namespace Eigen

#endif // AUTODIFF_HH
