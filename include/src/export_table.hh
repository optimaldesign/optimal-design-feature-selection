// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef EXPORT_TABLE_HH
#define EXPORT_TABLE_HH
// -- EXPORT TABLE/MATRIX AS CSV -----------------------------------------------
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

inline void export_table(std::string folder_name, std::string subfolder_name,
                         std::string file_name, std::vector<std::string> table,
                         int cols) {
  std::string folder = "results/" + folder_name + "/" + subfolder_name + "/";
  double rows_double = table.size() / (double)cols;
  // proof if rows is a natural numbers
  if (std::abs(rows_double - (int)rows_double) > 0) {
    throw std::invalid_argument("export table: wrong table dimensions. size " +
                                std::to_string(table.size()) + ", cols " +
                                std::to_string(cols) + ", rows " +
                                std::to_string(rows_double));
  }
  int rows = rows_double;
  // print csv to destination
  std::ofstream table_csv;
  table_csv.open(folder + file_name + ".csv");
  for (int row = 0; row < rows; ++row) {
    // cols
    table_csv << table[row * cols];
    for (int col = 1; col < cols; ++col) {
      table_csv << "," << table[row * cols + col];
    }
    table_csv << '\n';
  }
  table_csv.close();
}
// std::to_string has a limited precision
template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 100) {
  std::ostringstream out;
  out.precision(n);
  out << std::fixed << a_value;
  return out.str();
}

#endif // EXPORT_TABLE_HH
