// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef FEATURE_MAPS_HH
#define FEATURE_MAPS_HH
// -- FEATURE MAPS -------------------------------------------------------------
// Define custom automatic differentiation features
#include <Eigen/Core>
#include <autodiff.hh>
#include <cmath>
#include <linear_model.hh>
using namespace Eigen;

namespace FeatureMaps {
/** dette model features
 */
// exp(x^top*gen)
inline AD_Decl_Function exp(AD_Vector x, VectorXd gen) {
  AD_Decl_Function val = (AD_Decl_Function)1;
  for (int i = 0; i < x.size(); ++i) {
    AD_Decl_Function arg = x[i] * gen(i);
    val *= exp(arg);
  }
  return val;
};
// exp(-pow(x,gen))
inline AD_Decl_Function polynomial_monom(AD_Vector x, VectorXd gen);
inline AD_Decl_Function exp_minus_polynomial_monom(AD_Vector x, VectorXd gen) {
  return exp(-polynomial_monom(x, gen));
};
/** polynomial related
 * - monom: pow(x,gen)
 * - monom_shift: pow(x+a,b), gen = [a,b]
 */
inline AD_Decl_Function polynomial_monom(AD_Vector x, VectorXd gen) {
  return pow(x, gen);
};
inline AD_Decl_Function polynomial_monom_shift(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  VectorXd gen_shift = gen.head(x_size);
  VectorXd gen_exp = gen.segment(x_size, x_size);
  for (int i = 0; i < x_size; ++i) {
    x[i] += gen_shift(i);
  }
  return pow(x, gen_exp);
};
/** trigonometrical functions
 * - cos/sin/tan
 ** > scalar: cos(x^T * gen)
 ** > scalar_shift: cos(x^T * a + b), gen=[a,(scalar)b]
 ** > prod: prod_{i} cos(x(i)*gen(i))
 ** > prod_shift: prod_{i} cos(x(i)*a(i)+b(i)), gen = [a,b]
 */
inline AD_Decl_Function cos_scalar(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  AD_Decl_Function arg = (AD_Decl_Function)0;
  for (int i = 0; i < x_size; ++i) {
    arg += x[i] * gen(i);
  }
  return cos(arg);
};
inline AD_Decl_Function cos_scalar_shift(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  VectorXd a = gen.head(x_size);
  double b = gen(x_size);
  AD_Decl_Function x_a_b = (AD_Decl_Function)b;
  for (int i = 0; i < x_size; ++i) {
    x_a_b += x[i] * a(i);
  }
  return cos(x_a_b);
};
inline AD_Decl_Function cos_prod(AD_Vector x, VectorXd gen) {
  AD_Decl_Function val = (AD_Decl_Function)1;
  for (int i = 0; i < x.size(); ++i) {
    AD_Decl_Function arg = x[i] * gen(i);
    val *= cos(arg);
  }
  return val;
};
inline AD_Decl_Function cos_prod_shift(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  VectorXd a = gen.head(x_size);
  VectorXd b = gen.segment(x_size, x_size);
  AD_Decl_Function val = (AD_Decl_Function)1;
  for (int i = 0; i < x_size; ++i) {
    AD_Decl_Function arg = x[i] * a(i);
    arg += b(i);
    val *= cos(arg);
  }
  return val;
};
inline AD_Decl_Function sin_scalar(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  AD_Decl_Function arg = (AD_Decl_Function)0;
  for (int i = 0; i < x_size; ++i) {
    arg += x[i] * gen(i);
  }
  return sin(arg);
};
inline AD_Decl_Function sin_scalar_shift(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  VectorXd a = gen.head(x_size);
  double b = gen(x_size);
  AD_Decl_Function x_a_b = (AD_Decl_Function)b;
  for (int i = 0; i < x_size; ++i) {
    x_a_b += x[i] * a(i);
  }
  return sin(x_a_b);
};
inline AD_Decl_Function sin_prod(AD_Vector x, VectorXd gen) {
  AD_Decl_Function val = (AD_Decl_Function)1;
  for (int i = 0; i < x.size(); ++i) {
    AD_Decl_Function arg = x[i] * gen(i);
    val *= sin(arg);
  }
  return val;
};
inline AD_Decl_Function sin_prod_shift(AD_Vector x, VectorXd gen) {
  int x_size = x.size();
  VectorXd a = gen.head(x_size);
  VectorXd b = gen.segment(x_size, x_size);
  AD_Decl_Function val = (AD_Decl_Function)1;
  for (int i = 0; i < x_size; ++i) {
    AD_Decl_Function arg = x[i] * a(i);
    arg += b(i);
    val *= sin(arg);
  }
  return val;
};
}; // namespace FeatureMaps

#endif // FEATURE_MAPS_HH
