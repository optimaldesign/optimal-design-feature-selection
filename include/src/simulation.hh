// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef SIMULATION_HH
#define SIMULATION_HH

#include <Eigen/Core>
#include <adaptive_grid_optimal_design.hh>
#include <autodiff.hh>
#include <chrono>
#include <cmath>
#include <ctime>
#include <design_measure.hh>
#include <feature_maps.hh>
#include <interior_point_method.hh>
#include <iomanip>
#include <iostream>
#include <linear_model.hh>
#include <logging.hh>
#include <optimal_design_feature_selection.hh>
#include <random>
#include <string>

using namespace Eigen;
struct SimulationOption {
  // horizon parameter
  int m = 2;
  // target weights
  struct TargetWeights {
    std::vector<VectorXd> sequence;
    std::string type = "linear";
    double det_weight_min = 0.2;
    double det_weight_max = 0.6;
    double weight_measure_cost = 0.5;
  } target_weights;
  // cost function
  struct Cost {
    std::string type = "default";
    double delta = 2e1;
  } cost;
  // termination
  struct Termination {
    int cardinality = 5;
    int supp_number = 20;
  } termination;
  // oracle sigma
  struct Oracle {
    double sigma = 0.1;
  } oracle;
  // optional nullmodel choice
  struct Nullmodel {
    VectorXi feature_ids;
  } nullmodel;
  // export Adaptive Grid Optimal Design Method Hyperdata
  struct ExportOption {
    bool dispersion = false;
    bool weights = false;
  } export_option;
};

inline void define_nullmodel_from_feature_set(LinearModel &nullmodel,
                                              FeatureSet &feature_set,
                                              VectorXi ids) {
  for (int i = 0; i < ids.size(); ++i) {
    int id = ids(i);
    int feature_set_id;
    for (int k = 0; k < feature_set.size(); ++k) {
      if (id == feature_set[k].id) {
        // add feature to nullmodel and erase from feature set after loop
        nullmodel.add_feature(feature_set[k]);
        feature_set_id = k;
      }
    }
    feature_set.erase(feature_set.begin() + feature_set_id);
  }
}

inline void initial_proof_feature_set(FeatureSet feature_set, VectorXd x_l,
                                      VectorXd x_u, std::string simulation_id) {
  std::mt19937 gen(std::random_device{}());
  std::uniform_real_distribution<> dis(0, 1);
  int dim_size = x_u.size();
  VectorXd span = x_u - x_l;

  LinearModel linear_model;
  for (int i = 0; i < feature_set.size(); ++i) {
    linear_model.add_feature(feature_set[i]);
  }
  int feat_size = linear_model.feature_size();

  int iter;
  bool check;
  for (int i = 0; i < feat_size; ++i) {
    check = false;
    iter = 0;
    while (iter++ < 1e6 && !check) {
      VectorXd x_proof_point = x_l + dis(gen) * span;
      linear_model.set_x(x_proof_point);
      double linear_model_val_feat_i = linear_model.val(i);
      if (std::abs(linear_model_val_feat_i) > 0) {
        check = true;
      }
    }
    if (!check) {
      throw std::invalid_argument(
          "odfs initial feature set check in simulation " + simulation_id +
          ": feature with id " + std::to_string(feature_set[i].id) +
          " is a null-function, feat(x) = 0, for all x in domain");
    }
  }
}

inline void
parse_odfs_simulation_option(OptimalDesignFeatureSelectionOption &odfs_option,
                             SimulationOption simulation_option) {
  // declare options
  odfs_option.ccrit_horizon.m = simulation_option.m;
  odfs_option.target_weights.sequence =
      simulation_option.target_weights.sequence;
  odfs_option.target_weights.type = simulation_option.target_weights.type;
  odfs_option.target_weights.det_weight_min =
      simulation_option.target_weights.det_weight_min;
  odfs_option.target_weights.det_weight_max =
      simulation_option.target_weights.det_weight_max;
  odfs_option.target_weights.weight_measure_cost =
      simulation_option.target_weights.weight_measure_cost;
  odfs_option.cost.type = simulation_option.cost.type;
  odfs_option.cost.delta = simulation_option.cost.delta;
  odfs_option.oracle.sigma = simulation_option.oracle.sigma;
  // append options to simulation id
  odfs_option.folder_id += "__m_" + std::to_string(simulation_option.m) + "__";
  odfs_option.folder_id +=
      "targettype_" + simulation_option.target_weights.type + "__";
  odfs_option.folder_id +=
      "targetmin_" +
      std::to_string(simulation_option.target_weights.det_weight_min) + "__";
  odfs_option.folder_id +=
      "targetmax_" +
      std::to_string(simulation_option.target_weights.det_weight_max) + "__";
  odfs_option.folder_id +=
      "targetweightcost_" +
      std::to_string(simulation_option.target_weights.weight_measure_cost) +
      "__";
  odfs_option.folder_id +=
      "costdelta" + std::to_string(simulation_option.cost.delta) + "__";
  odfs_option.folder_id +=
      "oraclesigma" + std::to_string(simulation_option.oracle.sigma) + "__";
  // individual feature ids
  if (simulation_option.nullmodel.feature_ids.size() > 0) {
    odfs_option.folder_id +=
        "[" + std::to_string(simulation_option.nullmodel.feature_ids[0]);
    for (int i = 1; i < simulation_option.nullmodel.feature_ids.size(); ++i) {
      odfs_option.folder_id +=
          ";" + std::to_string(simulation_option.nullmodel.feature_ids[i]);
    }
    odfs_option.folder_id += "]";
  }
}
// Optimal Design Feature Selection
inline void odfs_run_simulation(SimulationOption simulation_option,
                                VectorXd x_l, VectorXd x_u, VectorXd x_q,
                                LinearModel oracle, std::string simulation_id,
                                LinearModel nullmodel, FeatureSet feature_set) {

  // optimal design feature selection options
  OptimalDesignFeatureSelectionOption odfs_option;
  odfs_option.bounds.x.l = x_l;
  odfs_option.bounds.x.u = x_u;
  odfs_option.grid.sampling = x_q;
  // termination
  odfs_option.termination.cardinality =
      simulation_option.termination.cardinality;
  odfs_option.termination.supp_number =
      simulation_option.termination.supp_number;
  // set oracle model with sigma
  odfs_option.oracle.linear_model = oracle;
  // define simulation id
  odfs_option.simulation_id = simulation_id;
  odfs_option.folder_id = simulation_id;
  // individual/general options to simulation
  parse_odfs_simulation_option(odfs_option, simulation_option);
  std::chrono::time_point<std::chrono::system_clock> now =
      std::chrono::system_clock::now();
  auto duration = now.time_since_epoch();
  auto timestamp_count =
      std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
  std::string timestamp = std::to_string(timestamp_count);
  odfs_option.folder_id += "__time_" + timestamp;
  std::string results_folder = "results/" + odfs_option.simulation_id + "/" +
                               odfs_option.folder_id + "/";
  std::string cmd = "mkdir -p '" + results_folder + "'";
  system(cmd.c_str());
  init_logging(results_folder);
  auto program_start = std::chrono::high_resolution_clock::now();
  // init odfs
  OptimalDesignFeatureSelection odfs_algorithm(odfs_option);
  odfs_algorithm.simulate(nullmodel, feature_set);
  odfs_algorithm.export_result();
  // last info: execution time in seconds
  auto program_end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> program_elapsed = program_end - program_start;
  logging::info("Execution Time: " + std::to_string(program_elapsed.count()) +
                " seconds");
}
// Optimal Design: D-Optimality
inline OptimalDesign
d_crit_run_simulation(AdapativeGridOptimalDesignOption d_option,
                      DesignMeasureGrid grid, LinearModel linear_model,
                      OptimalDesign given_design) {
  std::chrono::time_point<std::chrono::system_clock> now =
      std::chrono::system_clock::now();
  auto duration = now.time_since_epoch();
  auto timestamp_count =
      std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
  std::string timestamp = std::to_string(timestamp_count);
  d_option.folder_id = d_option.simulation_id + "__time_" + timestamp;
  std::string results_folder =
      "results/" + d_option.simulation_id + "/" + d_option.folder_id + "/";
  std::string cmd = "mkdir -p '" + results_folder + "'";
  system(cmd.c_str());
  init_logging(results_folder);
  auto program_start = std::chrono::high_resolution_clock::now();
  DesignOption design_option;
  design_option.design_type = "d-criterium";
  // d-crit linear model
  std::vector<LinearModel> design_linear_models;
  design_linear_models.push_back(linear_model);

  design_option.linear_models = design_linear_models;
  AdapativeGridOptimalDesign adaptive_grid_method(design_option, d_option);
  // export given design with measure
  logging::info("Export given design and its measure...");
  int given_supp_dim = given_design.supp.rows();
  if (given_supp_dim > 0) {
    std::vector<std::string> table_given_design_title{"x_0"};
    for (int i = 1; i < given_supp_dim; ++i) {
      table_given_design_title.push_back("x_" + std::to_string(i));
    }
    table_given_design_title.push_back("w");
    // write string
    std::vector<std::string> table_given_design = table_given_design_title;
    for (int i = 0; i < given_design.weights.size(); ++i) {
      for (int m = 0; m < given_supp_dim; ++m) {
        table_given_design.push_back(
            to_string_with_precision(given_design.supp.col(i)(m)));
      }
      table_given_design.push_back(
          to_string_with_precision(given_design.weights(i)));
    }
    // last line: measure
    table_given_design.push_back("measure");
    double given_design_measure = adaptive_grid_method.measure(given_design);
    table_given_design.push_back(
        to_string_with_precision(given_design_measure));
    for (int k = 0; k < given_supp_dim - 1; ++k) {
      table_given_design.push_back("");
    }
    export_table(d_option.simulation_id, d_option.folder_id,
                 "given_design_measure", table_given_design,
                 given_supp_dim + 1);
  }
  // export optimal design
  OptimalDesign optimal_design = adaptive_grid_method.solve(grid.supp);
  logging::info("Export optimal design...");
  int supp_dim = optimal_design.supp.col(0).size();
  std::vector<std::string> table_optimal_design_title{"x_0"};
  for (int i = 1; i < supp_dim; ++i) {
    table_optimal_design_title.push_back("x_" + std::to_string(i));
  }
  table_optimal_design_title.push_back("w");
  // write string
  std::vector<std::string> table_optimal_design = table_optimal_design_title;
  for (int i = 0; i < optimal_design.weights.size(); ++i) {
    for (int m = 0; m < supp_dim; ++m) {
      table_optimal_design.push_back(
          to_string_with_precision(optimal_design.supp.col(i)(m)));
    }
    table_optimal_design.push_back(
        to_string_with_precision(optimal_design.weights(i)));
  }
  export_table(d_option.simulation_id, d_option.folder_id, "optimal_design",
               table_optimal_design, supp_dim + 1);
  // last info: execution time in seconds
  auto program_end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> program_elapsed = program_end - program_start;
  logging::info("Execution Time: " + std::to_string(program_elapsed.count()) +
                " seconds");
  return optimal_design;
}
inline OptimalDesign
d_crit_run_simulation(AdapativeGridOptimalDesignOption d_option,
                      DesignMeasureGrid grid, LinearModel linear_model) {
  OptimalDesign given_design;
  given_design.supp = MatrixXd::Zero(0, 0);
  given_design.weights = VectorXd::Zero(0);
  return d_crit_run_simulation(d_option, grid, linear_model, given_design);
}
// =============================================================================
// SIMULATION: CONFIGURABLE
// =============================================================================
namespace Simulation {
const SimulationOption simulation_option_default;
/** Optimal Design Feature Selection Simulations */
void ODFS_Sinus(SimulationOption simulation_option = simulation_option_default);
void ODFS_Polynomial(
    SimulationOption simulation_option = simulation_option_default);
void ODFS_Test(SimulationOption simulation_option = simulation_option_default);
/** D-Optimality Tests
 * - Adaptive Grid Optimal Design: export Dispersionfunction, Weights
 */
/** Model index reference (Dette), see
 *
 * Duarte, Belmiro P., Weng Kee Wong und Holger Dette (2018).  Adaptive Grid
 * Semidefinite Programming for Finding Optimal Designs. In: Statistics and
 * Computing 28.2, S. 441–460. issn: 0960-3174. doi: 10.1007/s11222-017-9741-y.
 * url: https://doi.org/10.1007/ s11222-017-9741-y
 *
 **/
OptimalDesign Dette_Model_1(int q = 101, bool detailed = false);
OptimalDesign Dette_Model_2(int q = 101, bool detailed = false);
OptimalDesign Dette_Model_3(int q = 101, bool detailed = false);
OptimalDesign Dette_Model_4(int q = 101, bool detailed = false);
OptimalDesign Dette_Model_5(int q = 101, bool detailed = false);
// x dim: 2
OptimalDesign Dette_Model_9(int q = 21, bool detailed = false);
OptimalDesign Dette_Model_10(int q = 21, bool detailed = false);
OptimalDesign Dette_Model_11(int q = 21, bool detailed = false);
OptimalDesign Dette_Model_12(int q = 21, bool detailed = false);
OptimalDesign Dette_Model_13(int q = 21, bool detailed = false);
// x dim: 3
OptimalDesign Dette_Model_14(int q = 11, bool detailed = false);
// -- CUSTOM SIMULATION --------------------------------------------------------
OptimalDesign D_Crit_polynomial(int degree = 2, int q = 101,
                                bool detailed = false);
void D_Crit_polynomial_q_to_1001(int degree);
}; // namespace Simulation

#endif // SIMULATION_HH
