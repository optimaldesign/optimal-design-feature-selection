#ifndef LINEAR_REGRESSION_HH
#define LINEAR_REGRESSION_HH
// -- Linear Regression --------------------------------------------------------
// Fit = Find coefficients and get rmse related to trained data set
#include <Eigen/Core>
#include <adaptive_grid_optimal_design.hh>
#include <autodiff.hh>
#include <design_measure.hh>
#include <interior_point_method.hh>
// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <iostream>
#include <linear_model.hh>
#include <stdexcept>
#include <string>
using namespace Eigen;

struct Measurements {
  MatrixXd x;
  VectorXd y;
};

class LinearRegression {
public:
  LinearRegression(Measurements measurements_init) {
    measurements = measurements_init;
    meas_size = measurements.y.size();
  };
  void fit(LinearModel &linear_model);
  double get_rmse() { return rmse; }

private:
  Measurements measurements;
  int meas_size;
  double rmse;
};

#endif // LINEAR_REGRESSION_HH
