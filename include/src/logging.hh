// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef LOGGING_HH
#define LOGGING_HH

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/spdlog.h"
#include <chrono>
#include <ctime>
#include <iostream>
#include <string>

inline void init_logging(std::string results_folder) {
  try {
    std::chrono::time_point<std::chrono::system_clock> now =
        std::chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    auto timestamp_count =
        std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    std::string timestamp = std::to_string(timestamp_count);
    auto file_logger =
        spdlog::basic_logger_mt(results_folder, results_folder + "logfile.log");
    spdlog::set_default_logger(file_logger);
    spdlog::set_pattern("[%D - %H:%M:%S %z] [%n] [%l] [thread %t] %v");
  } catch (const spdlog::spdlog_ex &ex) {
    std::cout << "Log initialization failed: " << ex.what() << std::endl;
  }
}

inline void init_logging_ci() {
  try {
    std::chrono::time_point<std::chrono::system_clock> now =
        std::chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    auto timestamp_count =
        std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    std::string timestamp = std::to_string(timestamp_count);
    auto file_logger = spdlog::basic_logger_mt(
        "odfs_log", "logs/odfs_ci_log_file_" + timestamp + ".log");
    spdlog::set_default_logger(file_logger);
    spdlog::set_pattern("[%D - %H:%M:%S %z] [%n] [%l] [thread %t] %v");
  } catch (const spdlog::spdlog_ex &ex) {
    std::cout << "Log initialization failed: " << ex.what() << std::endl;
  }
}

namespace logging {
inline void info(std::string string_arg) { spdlog::info(string_arg); }
inline void info(int int_arg) { spdlog::info(std::to_string(int_arg)); }
} // namespace logging

#endif // LOGGING_HH
