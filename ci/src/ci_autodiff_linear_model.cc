// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include "catch.hpp"
#include <Eigen/Dense>
#include <autodiff.hh>
#include <iostream>
#include <linear_model.hh>
using namespace Eigen;

// define feature functions
AD_Decl_Function feature_0_function(AD_Vector x) {
  VectorXd exp_x(2);
  exp_x(0) = 2;
  exp_x(1) = 4;
  return pow(x, exp_x) - 1 / x[0];
}
AD_Decl_Function feature_1_function(AD_Vector x) { return -2 * x[0] * x[0]; }
AD_Decl_Function feature_2_function(AD_Vector x) {
  VectorXd exp_x(2);
  exp_x(0) = 0;
  exp_x(1) = 5;
  return x[0] - pow(x, exp_x);
}
AD_Decl_Function feature_3_function(AD_Vector x) {
  return sin(x[0] * x[1]) * exp(x[1] * cos(x[0]));
}

TEST_CASE("AD linear model", "[LinearModel]") {
  // declare features
  LinearModelFeature feature_0;
  feature_0.function = feature_0_function;
  feature_0.id = 0;
  LinearModelFeature feature_1;
  feature_1.function = feature_1_function;
  feature_1.id = 1;
  LinearModelFeature feature_2;
  feature_2.function = feature_2_function;
  feature_2.id = 2;
  LinearModelFeature feature_3;
  feature_3.function = feature_3_function;
  feature_3.id = 3;
  // add first two features
  LinearModel model;
  model.add_feature(feature_0);
  model.add_feature(feature_1);
  // coeff
  VectorXd coeff(2);
  coeff.setLinSpaced(1.0, 0.5);
  model.set_coeff(coeff);
  // x
  VectorXd x(2);
  x.setLinSpaced(2.0, 3.0);
  model.set_x(x);
  // check for correct value, gradient, hessian of linear model and features
  // =======================================================================
  SECTION("value, gradient, hessian of linear model") {
    // correct val, grad, hes
    double correct_val = 319.5;
    VectorXd correct_grad(2);
    correct_grad(0) = 320.25;
    correct_grad(1) = 432;
    MatrixXd correct_hes(2, 2);
    correct_hes(0, 0) = 159.75;
    correct_hes(0, 1) = 432;
    correct_hes(1, 0) = 432;
    correct_hes(1, 1) = 432;
    // requirements
    REQUIRE(correct_val == model.val());
    REQUIRE(correct_grad == model.grad());
    REQUIRE(correct_hes == model.hes());
  }
  SECTION("value, gradient, hessian of feature 0") {
    // correct val, grad, hes
    double correct_val = 323.5;
    VectorXd correct_grad(2);
    correct_grad(0) = 324.25;
    correct_grad(1) = 432;
    MatrixXd correct_hes(2, 2);
    correct_hes(0, 0) = 161.75;
    correct_hes(0, 1) = 432;
    correct_hes(1, 0) = 432;
    correct_hes(1, 1) = 432;
    // requirements
    REQUIRE(correct_val == model.val(0));
    REQUIRE(correct_grad == model.grad(0));
    REQUIRE(correct_hes == model.hes(0));
  }
  SECTION("value, gradient, hessian of feature 1") {
    // correct val, grad, hes
    double correct_val = -8;
    VectorXd correct_grad(2);
    correct_grad(0) = -8;
    correct_grad(1) = 0;
    MatrixXd correct_hes(2, 2);
    correct_hes(0, 0) = -4;
    correct_hes(0, 1) = 0;
    correct_hes(1, 0) = 0;
    correct_hes(1, 1) = 0;
    // requirements
    REQUIRE(correct_val == model.val(1));
    REQUIRE(correct_grad == model.grad(1));
    REQUIRE(correct_hes == model.hes(1));
  }
  // Remove feature with id 0 and add feature 2, check val, grad, hes of model
  // =========================================================================
  model.delete_feature(0);
  model.add_feature(feature_2);
  x.setLinSpaced(1.0, -2.0);
  model.set_x(x);
  coeff.setLinSpaced(0.5, 2);
  model.set_coeff(coeff);
  SECTION("remove and add new feature: proof value, gradient, hessian") {
    // correct val, grad, hes
    double correct_val = 65;
    VectorXd correct_grad(2);
    correct_grad(0) = 0;
    correct_grad(1) = -160;
    MatrixXd correct_hes(2, 2);
    correct_hes(0, 0) = -2;
    correct_hes(0, 1) = 0;
    correct_hes(1, 0) = 0;
    correct_hes(1, 1) = 320;
    // requirements
    REQUIRE(correct_val == model.val());
    REQUIRE(correct_grad == model.grad());
    REQUIRE(correct_hes == model.hes());
  }
  // Cause exceptions
  // ================
  SECTION("remove non-existing feature with id 0") {
    REQUIRE_THROWS(model.delete_feature(0));
  }
  SECTION("get value of non-existing feature with id 10") {
    REQUIRE_THROWS(model.val(10));
  }
  SECTION("get value of model with wrong size of coefficient") {
    LinearModel model_throw = model;
    VectorXd coeff_throw(3);
    coeff_throw.setLinSpaced(1, 3);
    model_throw.set_coeff(coeff_throw);
    REQUIRE_THROWS(model_throw.val());
  }
  SECTION("get non-existing feature") {
    auto features_id = model.features_id();
    REQUIRE_NOTHROW(model.feature(features_id[0]));
    REQUIRE_THROWS(model.feature(100));
  }
  SECTION("extended feature with sin, cos, exp") {
    model.delete_feature(1);
    model.add_feature(feature_3);
    REQUIRE_NOTHROW(model.set_x(x));
  }
}
