// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include <logging.hh>
int main(int argc, char *argv[]) {
  init_logging_ci();

  int result = Catch::Session().run(argc, argv);

  return result;
}
