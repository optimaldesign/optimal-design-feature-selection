// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <simulation.hh>
// -- SIMULATION ---------------------------------------------------------------
// Define own simulations for finding optimal designs of certain (quasi-)linear
// models or for reasearching in the method of optimal design feature selection.
namespace Simulation
{
  // -- Optimal Design Feature Selection Simulations -----------------------------
  /** ODFS_Sinus
 * X = [0,1]
 * Feature Set: sin(gen*x) with gen in [pi,2*pi,...,15*pi]
 * Oracle: Features = [1,3,6,7,14], Coeff = [1,-1,1,-1, 1];
 */
  void ODFS_Sinus(SimulationOption simulation_option)
  {
    // simulation id
    std::string simulation_id = "ODFS_Sinus";
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1.0;
    VectorXd x_l(x_dim);
    x_l << 0.0;
    VectorXd x_q(x_dim);
    int q = 201;
    x_q << q;
    // define general feature set
    FeatureSet feature_set;
    VectorXd gen(x_dim);
    // sin(gen*x): gen pi to 15*pi
    for (int i = 1; i <= 15; ++i)
    {
      LinearModelFeature feature_set_feat;
      feature_set_feat.function = FeatureMaps::sin_prod;
      feature_set_feat.id = i;
      gen << (double)i * M_PI;
      feature_set_feat.gen = gen;
      feature_set.push_back(feature_set_feat);
    }
    // proof correctness of feature set: all features, must exist min one x: f(x)
    // != 0
    initial_proof_feature_set(feature_set, x_u, x_l, simulation_id);
    // define standard model if option nullmodel is not set
    VectorXi nullmodel_ids;
    if (simulation_option.nullmodel.feature_ids.size() > 0)
    {
      nullmodel_ids = simulation_option.nullmodel.feature_ids;
    }
    else
    {
      VectorXi nullmodel_standard_ids(1);
      nullmodel_standard_ids << 1;
      nullmodel_ids = nullmodel_standard_ids;
    }
    LinearModel nullmodel;
    define_nullmodel_from_feature_set(nullmodel, feature_set, nullmodel_ids);
    // define oracle
    LinearModel oracle;
    LinearModelFeature oracle_feature;
    oracle_feature.function = FeatureMaps::sin_prod;
    VectorXd exponents(5);
    exponents << 1, 3, 6, 7, 14;
    for (int i = 0; i < exponents.size(); ++i)
    {
      oracle_feature.id = exponents(i);
      gen << exponents(i) * M_PI;
      oracle_feature.gen = gen;
      oracle.add_feature(oracle_feature);
    }

    VectorXd oracle_coeff(5);
    oracle_coeff << 1, -1, 1, -1, 1;
    oracle.set_coeff(oracle_coeff);
    // run simulation
    odfs_run_simulation(simulation_option, x_l, x_u, x_q, oracle, simulation_id,
                        nullmodel, feature_set);
  };
  /** ODFS_Polynomial
 * X = [0,1]
 * Feature Set: x^gen with gen in [-2,-1,0,...,10]
 * Oracle: Features = [-2, 0, 4, 8, 9], Coeff = [1, -5, 2, 2.2, -2];
 */
  void ODFS_Polynomial(SimulationOption simulation_option)
  {
    // simulation id
    std::string simulation_id = "ODFS_Polynomial";
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1;
    VectorXd x_l(x_dim);
    x_l << -1;
    VectorXd x_q(x_dim);
    int q = 401;
    x_q << q;
    // define general feature set
    FeatureSet feature_set;
    VectorXd gen(x_dim);
    // x^gen, gen in [-2,-1,0,...,10]
    for (int i = 0; i <= 10; ++i)
    {
      LinearModelFeature feature_set_feat;
      feature_set_feat.function = FeatureMaps::polynomial_monom;
      feature_set_feat.id = i;
      gen << (double)i;
      feature_set_feat.gen = gen;
      feature_set.push_back(feature_set_feat);
    }
    // proof correctness of feature set: all features, must exist min one x: f(x)
    // != 0
    initial_proof_feature_set(feature_set, x_u, x_l, simulation_id);
    // define standard model if option nullmodel is not set
    VectorXi nullmodel_ids;
    if (simulation_option.nullmodel.feature_ids.size() > 0)
    {
      nullmodel_ids = simulation_option.nullmodel.feature_ids;
    }
    else
    {
      VectorXi nullmodel_standard_ids(1);
      nullmodel_standard_ids << 1;
      nullmodel_ids = nullmodel_standard_ids;
    }
    LinearModel nullmodel;
    define_nullmodel_from_feature_set(nullmodel, feature_set, nullmodel_ids);
    // define oracle
    LinearModel oracle;
    LinearModelFeature oracle_feature;
    oracle_feature.function = FeatureMaps::polynomial_monom;
    VectorXd exponents(5);
    exponents << 0, 2, 3, 5, 9;
    for (int i = 0; i < exponents.size(); ++i)
    {
      oracle_feature.id = exponents(i);
      gen << exponents(i);
      oracle_feature.gen = gen;
      oracle.add_feature(oracle_feature);
    }

    VectorXd oracle_coeff(5);
    oracle_coeff << 2, 1, -1, 1, -1;
    oracle.set_coeff(oracle_coeff);
    // run simulation
    odfs_run_simulation(simulation_option, x_l, x_u, x_q, oracle, simulation_id,
                        nullmodel, feature_set);
  };

  /** ODFS_Polynomial
 * X = [0,1]
 * Feature Set: x^gen with gen in [-2,-1,0,...,10]
 * Oracle: Features = [-2, 0, 4, 8, 9], Coeff = [1, -5, 2, 2.2, -2];
 */
  void ODFS_Test(SimulationOption simulation_option)
  {
    // simulation id
    std::string simulation_id = "ODFS_Test";
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 2;
    VectorXd x_l(x_dim);
    x_l << 0;
    VectorXd x_q(x_dim);
    int q = 201;
    x_q << q;
    // define general feature set
    FeatureSet feature_set;
    VectorXd gen(x_dim);
    LinearModelFeature feature_set_feat;
    feature_set_feat.function = FeatureMaps::sin_scalar;
    feature_set_feat.id = 0;
    gen << M_PI;
    feature_set_feat.gen = gen;
    feature_set.push_back(feature_set_feat);
    feature_set_feat.function = FeatureMaps::cos_scalar;
    feature_set_feat.id = 1;
    gen << M_PI;
    feature_set_feat.gen = gen;
    feature_set.push_back(feature_set_feat);
    feature_set_feat.function = FeatureMaps::exp;
    feature_set_feat.id = 2;
    gen << -1;
    feature_set_feat.gen = gen;
    feature_set.push_back(feature_set_feat);
    feature_set_feat.function = FeatureMaps::exp;
    feature_set_feat.id = 3;
    gen << -2;
    feature_set_feat.gen = gen;
    feature_set.push_back(feature_set_feat);

    int prev_max_id = 4;
    // x^gen, gen in [-2,-1,0,...,10]
    for (int i = 0; i <= 8; ++i)
    {
      LinearModelFeature feature_set_feat;
      feature_set_feat.function = FeatureMaps::polynomial_monom;
      feature_set_feat.id = prev_max_id + i;
      gen << (double)i;
      feature_set_feat.gen = gen;
      feature_set.push_back(feature_set_feat);
    }
    // proof correctness of feature set: all features, must exist min one x: f(x)
    // != 0
    initial_proof_feature_set(feature_set, x_u, x_l, simulation_id);
    // define standard model if option nullmodel is not set
    VectorXi nullmodel_ids;
    if (simulation_option.nullmodel.feature_ids.size() > 0)
    {
      nullmodel_ids = simulation_option.nullmodel.feature_ids;
    }
    else
    {
      VectorXi nullmodel_standard_ids(1);
      nullmodel_standard_ids << 5;
      nullmodel_ids = nullmodel_standard_ids;
    }
    LinearModel nullmodel;
    define_nullmodel_from_feature_set(nullmodel, feature_set, nullmodel_ids);
    // define oracle: sin(2*pi*x)-cos(pi*x)+2exp(-5x)+2x^2+1
    LinearModel oracle;
    LinearModelFeature oracle_feature;
    oracle_feature.function = FeatureMaps::sin_scalar;
    gen << M_PI;
    oracle_feature.id = 0;
    oracle_feature.gen = gen;
    oracle.add_feature(oracle_feature);
    oracle_feature.function = FeatureMaps::exp;
    gen << -2;
    oracle_feature.id = 2;
    oracle_feature.gen = gen;
    oracle.add_feature(oracle_feature);
    oracle_feature.function = FeatureMaps::polynomial_monom;
    gen << 0;
    oracle_feature.id = 4;
    oracle_feature.gen = gen;
    oracle.add_feature(oracle_feature);
    oracle_feature.function = FeatureMaps::polynomial_monom;
    gen << 1;
    oracle_feature.id = 5;
    oracle_feature.gen = gen;
    oracle.add_feature(oracle_feature);
    oracle_feature.function = FeatureMaps::polynomial_monom;
    gen << 2;
    oracle_feature.id = 6;
    oracle_feature.gen = gen;
    oracle.add_feature(oracle_feature);

    VectorXd oracle_coeff(5);
    oracle_coeff << -2, 2, 1, -1, 2;
    oracle.set_coeff(oracle_coeff);
    // run simulation
    odfs_run_simulation(simulation_option, x_l, x_u, x_q, oracle, simulation_id,
                        nullmodel, feature_set);
  };

  // =============================================================================
  // D-Optimality
  // =============================================================================
  /** Model index reference (Dette), see
 *
 * Duarte, Belmiro P., Weng Kee Wong und Holger Dette (2018).  Adaptive Grid
 * Semidefinite Programming for Finding Optimal Designs. In: Statistics and
 * Computing 28.2, S. 441–460. issn: 0960-3174. doi: 10.1007/s11222-017-9741-y.
 * url: https://doi.org/10.1007/ s11222-017-9741-y
 *
 **/
  /** Table 1 (Duarte, Belmiro, Wong, Dette)
 * Battery of one factor statistical models
 **/
  // -----------------------------------------------------------------------------
  // Model 1 (Dette) - features: (1,x,x^2) - design space: [-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_1(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_1";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0;
    VectorXd x_q(x_dim);
    x_q << q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    for (int i = 0; i <= 2; ++i)
    {
      LinearModelFeature feature;
      feature.function = FeatureMaps::polynomial_monom;
      feature.id = i;
      VectorXd gen(x_dim);
      gen << (double)i;
      feature.gen = gen;
      linear_model.add_feature(feature);
    }
    // optional detailed trace of weights, dispersion,  measure
    if (detailed)
    {
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model);
    return optimal_design;
  };
  // -----------------------------------------------------------------------------
  // Model 2 (Dette) - features: (1,x,x^2,x^3) - design space: [-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_2(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_2";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0;
    VectorXd x_q(x_dim);
    x_q << q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    for (int i = 0; i <= 3; ++i)
    {
      LinearModelFeature feature;
      feature.function = FeatureMaps::polynomial_monom;
      feature.id = i;
      VectorXd gen(x_dim);
      gen << (double)i;
      feature.gen = gen;
      linear_model.add_feature(feature);
    }
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int supp_number = 4;
      given_design.weights = VectorXd::Constant(4, 0.25);
      given_design.supp = MatrixXd(x_dim, supp_number);
      given_design.supp.col(0) << -1.0;
      given_design.supp.col(1) << -0.45;
      given_design.supp.col(2) << 0.45;
      given_design.supp.col(3) << 1.0;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  };
  // -----------------------------------------------------------------------------
  // Model 3 (Dette) - features: (1,x,x^2,x^3,x^4) - design space: [-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_3(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_3";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0;
    VectorXd x_q(x_dim);
    x_q << q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    for (int i = 0; i <= 4; ++i)
    {
      LinearModelFeature feature;
      feature.function = FeatureMaps::polynomial_monom;
      feature.id = i;
      VectorXd gen(x_dim);
      gen << (double)i;
      feature.gen = gen;
      linear_model.add_feature(feature);
    }
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 5;
      given_design.weights = VectorXd::Constant(given_supp_number, 0.2);
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1.0;
      given_design.supp.col(1) << -0.6501;
      given_design.supp.col(2) << 0;
      given_design.supp.col(3) << 0.6501;
      given_design.supp.col(4) << 1.0;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  };
  // -----------------------------------------------------------------------------
  // Model 4 (Dette) - features: (1,x,x^2,x^3,x^4,x^5) - design space: [-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_4(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_4";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0;
    VectorXd x_q(x_dim);
    x_q << q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    for (int i = 0; i <= 5; ++i)
    {
      LinearModelFeature feature;
      feature.function = FeatureMaps::polynomial_monom;
      feature.id = i;
      VectorXd gen(x_dim);
      gen << (double)i;
      feature.gen = gen;
      linear_model.add_feature(feature);
    }
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 6;
      given_design.weights = VectorXd::Constant(given_supp_number, 1.0 / 6.0);
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1.0;
      given_design.supp.col(1) << -0.7688;
      given_design.supp.col(2) << -0.29;
      given_design.supp.col(3) << 0.29;
      given_design.supp.col(4) << 0.7688;
      given_design.supp.col(5) << 1.0;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -----------------------------------------------------------------------------
  // Model 5 (Dette) - features: (1,x,x^-1,exp(-1)) - design space: [0.5,2.5]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_5(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_5";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 2.5;
    VectorXd x_l(x_dim);
    x_l << 0.5;
    VectorXd x_q(x_dim);
    x_q << q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    // x^-1, 1, x^1
    for (int i = 0; i <= 2; ++i)
    {
      LinearModelFeature feature;
      feature.function = FeatureMaps::polynomial_monom;
      feature.id = i;
      VectorXd gen(x_dim);
      gen << (double)i - 1;
      feature.gen = gen;
      linear_model.add_feature(feature);
    }
    // exp(-x)
    LinearModelFeature feature;
    feature.function = FeatureMaps::exp;
    feature.id = 3;
    VectorXd gen(x_dim);
    gen << -1.0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 4;
      given_design.weights = VectorXd::Constant(given_supp_number, 0.25);
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << 0.5;
      given_design.supp.col(1) << 0.7773;
      given_design.supp.col(2) << 1.5800;
      given_design.supp.col(3) << 2.5;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  };
  /** Table 8 (Duarte, Belmiro, Wong, Dette)
 * Battery of multiple factor statistical models
 **/
  // -----------------------------------------------------------------------------
  // Model 9 (Dette)
  // - features: (1,x_1,x_2,x_1^2,x_2^2)
  // - design space: [-1,1]x[-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_9(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_9";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 2;
    VectorXd x_u(x_dim);
    x_u << 1.0, 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0, -1.0;
    VectorXd x_q(x_dim);
    x_q << q, q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    LinearModelFeature feature;
    VectorXd gen(x_dim);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 0;
    gen << 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 1;
    gen << 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 2;
    gen << 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 3;
    gen << 2, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 4;
    gen << 0, 2;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 9;
      given_design.weights = VectorXd::Constant(given_supp_number, 1.0 / 9.0);
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1, -1;
      given_design.supp.col(1) << -1, 0;
      given_design.supp.col(2) << -1, 1;
      given_design.supp.col(3) << 0, -1;
      given_design.supp.col(4) << 0, 0;
      given_design.supp.col(5) << 0, 1;
      given_design.supp.col(6) << 1, -1;
      given_design.supp.col(7) << 1, 0;
      given_design.supp.col(8) << 1, 1;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
      d_option.trace.dispersion_sampling_per_dim =
          (int)std::pow(1e3, (double)1 / x_dim);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -----------------------------------------------------------------------------
  // Model 10 (Dette)
  // - features: (1,x_1,x_2,x_1^2,x_2^2,x_1*x_2)
  // - design space: [-1,1]x[-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_10(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_10";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 2;
    VectorXd x_u(x_dim);
    x_u << 1.0, 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0, -1.0;
    VectorXd x_q(x_dim);
    x_q << q, q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    LinearModelFeature feature;
    VectorXd gen(x_dim);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 0;
    gen << 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 1;
    gen << 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 2;
    gen << 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 3;
    gen << 2, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 4;
    gen << 0, 2;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 5;
    gen << 1, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 9;
      given_design.weights = VectorXd(9);
      given_design.weights << 0.1458, 0.0802, 0.1458, 0.0802, 0.0962, 0.0802,
          0.1458, 0.0802, 0.1458;
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1, -1;
      given_design.supp.col(1) << -1, 0;
      given_design.supp.col(2) << -1, 1;
      given_design.supp.col(3) << 0, -1;
      given_design.supp.col(4) << 0, 0;
      given_design.supp.col(5) << 0, 1;
      given_design.supp.col(6) << 1, -1;
      given_design.supp.col(7) << 1, 0;
      given_design.supp.col(8) << 1, 1;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
      d_option.trace.dispersion_sampling_per_dim =
          (int)std::pow(1e3, (double)1 / x_dim);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -----------------------------------------------------------------------------
  // Model 11 (Dette)
  // - features: (1,x_1,x_2,exp(-x_1),exp(-x_2))
  // - design space: [-1,1]x[-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_11(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_11";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 2;
    VectorXd x_u(x_dim);
    x_u << 1.0, 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0, -1.0;
    VectorXd x_q(x_dim);
    x_q << q, q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    LinearModelFeature feature;
    VectorXd gen(x_dim);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 0;
    gen << 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 1;
    gen << 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 2;
    gen << 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.function = FeatureMaps::exp;
    feature.id = 3;
    gen << -1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 4;
    gen << 0, -1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 9;
      given_design.weights = VectorXd::Constant(given_supp_number, 1.0 / 9.0);
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1, -1;
      given_design.supp.col(1) << -1, -0.1650;
      given_design.supp.col(2) << -1, 1;
      given_design.supp.col(3) << -0.1650, -1;
      given_design.supp.col(4) << -0.1650, -0.1650;
      given_design.supp.col(5) << -0.1650, 1;
      given_design.supp.col(6) << 1, -1;
      given_design.supp.col(7) << 1, -0.1650;
      given_design.supp.col(8) << 1, 1;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
      d_option.trace.dispersion_sampling_per_dim =
          (int)std::pow(1e3, (double)1 / x_dim);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -----------------------------------------------------------------------------
  // Model 12 (Dette)
  // - features: (1,x_1,x_2,exp(-x_1),exp(-x_2),x_1*x_2)
  // - design space: [-1,1]x[-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_12(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_12";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 2;
    VectorXd x_u(x_dim);
    x_u << 1.0, 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0, -1.0;
    VectorXd x_q(x_dim);
    x_q << q, q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    LinearModelFeature feature;
    VectorXd gen(x_dim);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 0;
    gen << 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 1;
    gen << 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 2;
    gen << 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.function = FeatureMaps::exp;
    feature.id = 3;
    gen << -1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 4;
    gen << 0, -1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 5;
    gen << 1, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 9;
      given_design.weights = VectorXd(9);
      given_design.weights << 0.1357, 0.0834, 0.1444, 0.0834, 0.0796, 0.0956,
          0.1444, 0.0796, 0.1539;
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1, -1;
      given_design.supp.col(1) << -1, -0.1966;
      given_design.supp.col(2) << -1, 1;
      given_design.supp.col(3) << -0.1966, -1;
      given_design.supp.col(4) << -0.2027, 1;
      given_design.supp.col(5) << -0.1537, -0.1537;
      given_design.supp.col(6) << 1, -1;
      given_design.supp.col(7) << 1, -0.2027;
      given_design.supp.col(8) << 1, 1;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
      d_option.trace.dispersion_sampling_per_dim =
          (int)std::pow(1e3, (double)1 / x_dim);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -----------------------------------------------------------------------------
  // Model 13 (Dette)
  // - features: (1,x_1,x_2,exp(-x_1),exp(-x_2),exp(-x_1*x_2))
  // - design space: [-1,1]x[-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_13(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_13";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 2;
    VectorXd x_u(x_dim);
    x_u << 1.0, 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0, -1.0;
    VectorXd x_q(x_dim);
    x_q << q, q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    LinearModelFeature feature;
    VectorXd gen(x_dim);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 0;
    gen << 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 1;
    gen << 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 2;
    gen << 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.function = FeatureMaps::exp;
    feature.id = 3;
    gen << -1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 4;
    gen << 0, -1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.function = FeatureMaps::exp_minus_polynomial_monom;
    feature.id = 5;
    gen << 1, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 9;
      given_design.weights = VectorXd(9);
      given_design.weights << 0.1324, 0.0852, 0.1480, 0.0853, 0.0977, 0.0774,
          0.1480, 0.0775, 0.1486;
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1, -1;
      given_design.supp.col(1) << -1, -0.2060;
      given_design.supp.col(2) << -1, 1;
      given_design.supp.col(3) << -0.2060, -1;
      given_design.supp.col(4) << -0.1484, -0.1484;
      given_design.supp.col(5) << -0.1674, 1;
      given_design.supp.col(6) << 1, -1;
      given_design.supp.col(7) << 1, -0.1674;
      given_design.supp.col(8) << 1, 1;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
      d_option.trace.dispersion_sampling_per_dim =
          (int)std::pow(1e3, (double)1 / x_dim);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -----------------------------------------------------------------------------
  // Model 14 (Dette)
  // - features: (1,x_1,x_2,x_3,x_1^2,x_2^2,x_3^2,x_1*x_2,x_1*x_3,x_2*x_3)
  // - design space: [-1,1]x[-1,1]x[-1,1]
  // -----------------------------------------------------------------------------
  OptimalDesign Dette_Model_14(int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // simulation id
    d_option.simulation_id = "Dette_Model_14";
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 3;
    VectorXd x_u(x_dim);
    x_u << 1.0, 1.0, 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0, -1.0, -1.0;
    VectorXd x_q(x_dim);
    x_q << q, q, q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    LinearModelFeature feature;
    VectorXd gen(x_dim);
    feature.function = FeatureMaps::polynomial_monom;
    feature.id = 0;
    gen << 0, 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 1;
    gen << 1, 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 2;
    gen << 0, 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 3;
    gen << 0, 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 4;
    gen << 2, 0, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 5;
    gen << 0, 2, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 6;
    gen << 0, 0, 2;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 7;
    gen << 1, 1, 0;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 8;
    gen << 1, 0, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    feature.id = 9;
    gen << 0, 1, 1;
    feature.gen = gen;
    linear_model.add_feature(feature);
    // optional given design
    // optional detailed trace of weights, dispersion,  measure
    OptimalDesign given_design;
    if (detailed)
    {
      // solution optimal design by dette
      int given_supp_number = 27;
      given_design.weights = VectorXd(given_supp_number);
      given_design.weights << 0.0690, 0.0249, 0.0690, 0.0249, 0.0209, 0.0249,
          0.0690, 0.0249, 0.0690, 0.0249, 0.0209, 0.0249, 0.0209, 0.0237, 0.0209,
          0.0249, 0.0209, 0.0249, 0.0690, 0.0249, 0.0690, 0.0249, 0.0210, 0.0249,
          0.0690, 0.0249, 0.0690;
      given_design.supp = MatrixXd(x_dim, given_supp_number);
      given_design.supp.col(0) << -1, -1, -1;
      given_design.supp.col(1) << -1, -1, 0;
      given_design.supp.col(2) << -1, -1, 1;
      given_design.supp.col(3) << -1, 0, -1;
      given_design.supp.col(4) << -1, 0, 0;
      given_design.supp.col(5) << -1, 0, 1;
      given_design.supp.col(6) << -1, 1, -1;
      given_design.supp.col(7) << -1, 1, 0;
      given_design.supp.col(8) << -1, 1, 1;
      given_design.supp.col(9) << 0, -1, -1;
      given_design.supp.col(10) << 0, -1, 0;
      given_design.supp.col(11) << 0, -1, 1;
      given_design.supp.col(12) << 0, 0, -1;
      given_design.supp.col(13) << 0, 0, 0;
      given_design.supp.col(14) << 0, 0, 1;
      given_design.supp.col(15) << 0, 1, -1;
      given_design.supp.col(16) << 0, 1, 0;
      given_design.supp.col(17) << 0, 1, 1;
      given_design.supp.col(18) << 1, -1, -1;
      given_design.supp.col(19) << 1, -1, 0;
      given_design.supp.col(20) << 1, -1, 1;
      given_design.supp.col(21) << 1, 0, -1;
      given_design.supp.col(22) << 1, 0, 0;
      given_design.supp.col(23) << 1, 0, 1;
      given_design.supp.col(24) << 1, 1, -1;
      given_design.supp.col(25) << 1, 1, 0;
      given_design.supp.col(26) << 1, 1, 1;
      // trace options for detailed information
      d_option.trace.weights = true;
      d_option.trace.dispersion = true;
      d_option.trace.measure = true;
      d_option.trace.dispersion_sampling_per_dim =
          (int)std::pow(1e3, (double)1 / x_dim);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model, given_design);
    return optimal_design;
  }
  // -- CUSTOM SIMULATION --------------------------------------------------------
  /** D_Crit_polynomial
 * Find an optimal design of a polynomial of a certain degree with an optional
 * for a detailed trace of weights, dispersion functions und measures.
 */
  OptimalDesign D_Crit_polynomial(int degree, int q, bool detailed)
  {
    AdapativeGridOptimalDesignOption d_option;
    // trace options for detailed information
    d_option.trace.weights = detailed;
    d_option.trace.dispersion = detailed;
    d_option.trace.measure = detailed;
    // simulation id
    d_option.simulation_id = "D_CRIT_polynomial";
    d_option.simulation_id += "__degree_" + std::to_string(degree);
    d_option.simulation_id += "__sampling_" + std::to_string(q);
    // x-bounds
    int x_dim = 1;
    VectorXd x_u(x_dim);
    x_u << 1.0;
    VectorXd x_l(x_dim);
    x_l << -1.0;
    VectorXd x_q(x_dim);
    x_q << q;
    d_option.supp.minimize.bounds.x.u = x_u;
    d_option.supp.minimize.bounds.x.l = x_l;
    // Grid
    DesignMeasureGrid grid(x_l, x_u, x_q);
    // linear model
    LinearModel linear_model;
    for (int i = 0; i <= degree; ++i)
    {
      LinearModelFeature feature;
      feature.function = FeatureMaps::polynomial_monom;
      feature.id = i;
      VectorXd gen(x_dim);
      gen << (double)i;
      feature.gen = gen;
      linear_model.add_feature(feature);
    }
    // solve d-crit optimal design
    OptimalDesign optimal_design =
        d_crit_run_simulation(d_option, grid, linear_model);
    return optimal_design;
  };
  /** D_Crit_polynomial_q_to_1001
 * Get the measure and execution time in seconds in dependence of the sampling
 * points number q.
 */
  void D_Crit_polynomial_q_to_1001(int degree)
  {
    std::vector<int> q_vec;
    for (int q = degree + 1; q <= 51; q += 1)
    {
      q_vec.push_back(q);
    }
    for (int q = 53; q <= 101; q += 2)
    {
      q_vec.push_back(q);
    }
    for (int q = 121; q <= 501; q += 20)
    {
      q_vec.push_back(q);
    }
    for (int q = 551; q <= 1001; q += 50)
    {
      q_vec.push_back(q);
    }
    std::cout << "sampling,measure,execution_time" << '\n';
    for (auto &&q : q_vec)
    {
      auto start = std::chrono::high_resolution_clock::now();
      OptimalDesign optimal_design = D_Crit_polynomial(degree, q);
      double measure = optimal_design.measure;
      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> elapsed = end - start;
      std::cout << q << "," << measure << "," << elapsed.count() << '\n';
    }
  };
}; // namespace Simulation
