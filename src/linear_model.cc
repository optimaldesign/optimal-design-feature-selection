// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <linear_model.hh>
using namespace Eigen;
// -- LINEAR MODEL -------------------------------------------------------------
// Perform actions on a linear model as set value x, get coefficients,
// add/delete a feature,...
// Features are defined as an automatic differentiation feature.
// set/get x vector
void LinearModel::set_x(VectorXd arg) {
  // update x
  x = arg;
  // update ad features
  features_size = decl_features.size();
  features.clear();
  for (int i = 0; i < features_size; ++i) {
    features.push_back(
        AD_Function(decl_features[i].function, x, decl_features[i].gen));
  }
}
VectorXd LinearModel::get_x() { return x; }
// set/get coefficients
void LinearModel::set_coeff(VectorXd arg) { coeff = arg; }
VectorXd LinearModel::get_coeff() { return coeff; }
// add/delete/get feature
void LinearModel::add_feature(LinearModelFeature new_feature) {
  // proof "id" integrity
  bool integrity = true;
  for (int i = 0; i < features_size; ++i) {
    if (new_feature.id == decl_features[i].id) {
      integrity = false;
    }
  }
  if (!integrity) {
    throw std::invalid_argument(
        "add feature: id integrity violated, new feature planned with id " +
        std::to_string(new_feature.id));
  }
  features_size++;
  decl_features.push_back(new_feature);
}
void LinearModel::delete_feature(int feat_id) {
  int pos = -1;
  for (int i = 0; i < features_size; ++i) {
    if (decl_features[i].id == feat_id) {
      pos = i;
    }
  }
  if (pos == -1) {
    throw std::invalid_argument("delete feature: id not in feature set");
  } else {
    decl_features.erase(decl_features.begin() + pos);
    features_size -= 1;
  }
}
LinearModelFeature LinearModel::feature(int feat_id) {
  int pos = -1;
  for (int i = 0; i < features_size; ++i) {
    if (decl_features[i].id == feat_id) {
      pos = i;
    }
  }
  if (pos == -1) {
    throw std::invalid_argument("get feature: id not in feature set");
  } else {
    return decl_features[pos];
  }
}
// get feature ids
std::vector<int> LinearModel::features_id() {
  std::vector<int> features_id(features_size);
  for (int i = 0; i < features_id.size(); ++i) {
    features_id[i] = decl_features[i].id;
  }
  return features_id;
}
// value of model or features
double LinearModel::val(int feat_id) {
  proof_feature_size();
  double value = 0;
  if (-1 == feat_id) {
    proof_coeff_size();
    for (int i = 0; i < features_size; ++i) {
      value += coeff[i] * features[i].val();
    }
  } else if (0 <= feat_id && features_size > feat_id) {
    value = features[feat_id].val();
  } else {
    throw std::invalid_argument("model val: feat_id out of range");
  }
  return value;
}
// gradient of model or features
VectorXd LinearModel::grad(int feat_id) {
  proof_feature_size();
  VectorXd gradient = VectorXd::Zero(x.size());
  if (-1 == feat_id) {
    proof_coeff_size();
    for (int i = 0; i < features_size; ++i) {
      gradient += coeff[i] * features[i].grad();
    }
  } else if (0 <= feat_id && features_size > feat_id) {
    gradient = features[feat_id].grad();
  } else {
    throw std::invalid_argument("model grad: feat_id out of range");
  }
  return gradient;
}
// hessian of model or features
MatrixXd LinearModel::hes(int feat_id) {
  proof_feature_size();
  MatrixXd hessian = MatrixXd::Zero(x.size(), x.size());
  if (-1 == feat_id) {
    proof_coeff_size();
    for (int i = 0; i < features_size; ++i) {
      hessian += coeff[i] * features[i].hes();
    }
  } else if (0 <= feat_id && features_size > feat_id) {
    hessian = features[feat_id].hes();
  } else {
    throw std::invalid_argument("model hes: feat_id out of range");
  }
  return hessian;
}

void LinearModel::proof_coeff_size() {
  if (coeff.size() != features_size) {
    throw std::invalid_argument(
        "model val/grad/hes: coeff (size " + std::to_string(coeff.size()) +
        ") has not the correct size (" + std::to_string(features_size) + ")");
  }
}
// proof feature size
void LinearModel::proof_feature_size() {
  if (features_size == 0) {
    throw std::invalid_argument("model val/grad/hes: no features in model yet");
  }
}

// design matrix row-wise features at x
VectorXd LinearModel::design() {
  VectorXd design_vector(features_size);
  for (int i = 0; i < features_size; ++i) {
    design_vector(i) = features[i].val();
  }
  return design_vector;
}

// design matrix derivation i,j row-wise features at x
VectorXd LinearModel::design_deriv(int i, int j) {
  VectorXd design_deriv_vector(features_size);
  if (-1 == j) {
    for (int k = 0; k < features_size; ++k) {
      design_deriv_vector(k) = features[k].grad()(i);
    }
  } else if (-1 < j) {
    for (int k = 0; k < features_size; ++k) {
      design_deriv_vector(k) = features[k].hes()(i, j);
    }
  }
  return design_deriv_vector;
}

// jacobian matrix
MatrixXd LinearModel::jac() {
  MatrixXd jacobian(features_size, x.size());
  for (int i = 0; i < features_size; ++i) {
    jacobian.row(i) = features[i].grad();
  }
  return jacobian;
}
int LinearModel::feature_size() { return features_size; }
