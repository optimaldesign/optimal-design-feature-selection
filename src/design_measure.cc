// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <Eigen/Dense>
#include <design_measure.hh>
using namespace Eigen;
// -- DESIGN MEASURE -----------------------------------------------------------
// Measures: C-,D-,Xi-Optimality

// Building initial grid for adaptive grid optimization in dependence of a given
// set of existing support vectors.
void DesignMeasureGrid::build_supp(ArrayXd pos, int d, ArrayXd x) {
  int dim = x_u.size();
  ArrayXd delta_x = (x_u - x_l) / (x_q - 1);
  if (d < dim) {
    for (int i = 0; i < x_q(d); ++i) {
      x(d) = x_l(d) + i * delta_x(d);
      pos(d) = i;
      build_supp(pos, d + 1, x);
    }
  } else {
    int supp_pos = 0;
    int helper = 1;
    for (int i = 0; i < dim; ++i) {
      supp_pos += pos(i) * helper;
      helper *= x_q(i);
    }
    supp.col(supp_pos) = x;
    // proof collision to measured supp
    if (measured_supp_set) {
      for (int i = 0; i < measured_supp.cols(); ++i) {
        for (int j = 0; j < measured_supp_ids.size(); ++j) {
          int sup_found_col = measured_supp_ids(j);
          if (-1 == sup_found_col) {
            // compare distance x to supp col i
            VectorXd x_grid = x;
            VectorXd diff = x_grid - measured_supp.col(i);
            double dist = diff.norm();
            if (dist < 1e-8) {
              measured_supp_ids(i) = supp_pos;
            }
          }
        }
      }
    }
  }
}

/** Helper
 * - design matrix
 * - Normalized Fisher Information Matrix
 */
MatrixXd DesignMeasureHelper::design_matrix(MatrixXd &supp_points,
                                            LinearModel &linear_model) {
  int supp_size = supp_points.cols();
  int feature_size = linear_model.feature_size();
  MatrixXd design = MatrixXd::Zero(supp_size, feature_size);
  for (int i = 0; i < supp_size; ++i) {
    linear_model.set_x(supp_points.col(i));
    design.row(i) = linear_model.design();
  }
  return design;
}

MatrixXd DesignMeasureHelper::norm_fim(VectorXd &weights, MatrixXd &design) {
  int fim_size = design.cols();
  int w_size = weights.size();
  // build normalized fisher information matrix
  MatrixXd fim = MatrixXd::Zero(fim_size, fim_size);
  for (int i = 0; i < w_size; ++i) {
    fim += weights(i) * design.row(i).transpose() * design.row(i);
  }
  return fim;
}

/** Determinant Criterium
 * - weights dependend
 * - support point dependend
 */
FunctionTargetResult
determinant_criterium_weights(VectorXd &weights, VectorXd &vec_arg,
                              MatrixXd &design,
                              std::vector<LinearModel> &linear_models) {
  int w_size = weights.size();
  DesignMeasureHelper helper;
  MatrixXd fim = helper.norm_fim(weights, design);
  MatrixXd phi = design * fim.lu().solve(design.transpose());
  // -log(det(fim))
  FunctionTargetResult rslt;
  rslt.value = -std::log(fim.determinant());
  // gradient/hessian
  rslt.gradient = VectorXd::Zero(w_size);
  for (int i = 0; i < w_size; ++i) {
    rslt.gradient(i) = -phi(i, i);
  }
  rslt.hessian = phi.cwiseProduct(phi);
  return rslt;
}

FunctionTargetResult
determinant_criterium_supp(VectorXd &x, VectorXd &vec_arg, MatrixXd &fim,
                           std::vector<LinearModel> &linear_models) {
  int x_size = x.size();
  // only one lin model in this criterium
  LinearModel lin_model = linear_models[0];
  int feat_size = lin_model.feature_size();
  // set x to linear model
  lin_model.set_x(x);
  // design vector
  VectorXd design_vec = lin_model.design();
  // tmp inv(fim)*design_vec
  VectorXd inv_fim_design_vec = fim.lu().solve(design_vec);
  // jacobian matrix
  MatrixXd jacobian = lin_model.jac();
  // calculate results of value, gradient
  FunctionTargetResult rslt;
  rslt.value = -design_vec.transpose() * inv_fim_design_vec + feat_size;
  rslt.gradient = -jacobian.transpose() * inv_fim_design_vec;
  // calculate hessian
  rslt.hessian = -jacobian.transpose() * fim.lu().solve(jacobian);
  for (int i = 0; i < x_size; ++i) {
    for (int j = 0; j <= i; ++j) {
      rslt.hessian(i, j) -=
          lin_model.design_deriv(i, j).transpose() * inv_fim_design_vec;
      if (j < i) {
        rslt.hessian(j, i) = rslt.hessian(i, j);
      }
    }
  }
  return rslt;
}
/** C-Criterium
 * - weights dependend
 * - support point dependend
 */
FunctionTargetResult
c_criterium_weights(VectorXd &weights, VectorXd &vec_arg, MatrixXd &design,
                    std::vector<LinearModel> &linear_models) {
  int m = vec_arg(0);
  int w_size = weights.size();
  int feat_size = design.cols();
  DesignMeasureHelper helper;
  MatrixXd fim = helper.norm_fim(weights, design);
  VectorXd e_n = VectorXd::Constant(feat_size, 1);
  if (m != feat_size) {
    e_n.head(feat_size - m) = VectorXd::Zero(feat_size - m);
  }
  VectorXd fim_e_n = fim.lu().solve(e_n);
  VectorXd theta = design * fim_e_n;
  double phi = e_n.transpose() * fim_e_n;
  FunctionTargetResult rslt;
  rslt.value = std::log(phi);
  // gradient/hessian
  rslt.gradient = -1 / phi * theta.cwiseProduct(theta);

  rslt.hessian = rslt.gradient * rslt.gradient.transpose() +
                 2 / phi *
                     (design * fim.lu().solve(design.transpose()))
                         .cwiseProduct(theta * theta.transpose());
  return rslt;
}

FunctionTargetResult c_criterium_supp(VectorXd &x, VectorXd &vec_arg,
                                      MatrixXd &fim,
                                      std::vector<LinearModel> &linear_models) {
  int x_size = x.size();
  LinearModel lin_model = linear_models[0];
  lin_model.set_x(x);
  int feat_size = fim.rows();
  int m = vec_arg(0);
  VectorXd e_n = VectorXd::Constant(feat_size, 1);
  if (m != feat_size) {
    e_n.head(feat_size - m) = VectorXd::Zero(feat_size - m);
  }
  VectorXd design_vec = lin_model.design();
  MatrixXd jacobian = lin_model.jac();
  VectorXd fim_e_n = fim.lu().solve(e_n);
  double phi = e_n.transpose() * fim_e_n;
  double theta = design_vec.transpose() * fim_e_n;
  VectorXd jac_fim_e_n = jacobian.transpose() * fim_e_n;
  FunctionTargetResult rslt;
  rslt.value = phi - std::pow(theta, 2);
  rslt.gradient = -theta * jac_fim_e_n;
  rslt.hessian = -jac_fim_e_n * jac_fim_e_n.transpose();
  for (int i = 0; i < x_size; ++i) {
    for (int j = 0; j <= i; ++j) {
      rslt.hessian(i, j) -=
          theta * lin_model.design_deriv(i, j).transpose() * fim_e_n;
      if (j < i) {
        rslt.hessian(j, i) = rslt.hessian(i, j);
      }
    }
  }
  return rslt;
}

// cost function
// default
FunctionTargetResult
cost_weight_default(VectorXd &weights, VectorXd &delta_vec, MatrixXd &vec_mat,
                    std::vector<LinearModel> &linear_models) {
  double delta = delta_vec(delta_vec.size() - 1);
  VectorXd measured_supp_ids = delta_vec.head(delta_vec.size() - 1);

  ArrayXd phi(weights.size());
  ArrayXd delta_phi(weights.size());
  VectorXd hes_diag(weights.size());
  for (int i = 0; i < weights.size(); ++i) {
    phi(i) = 1 + delta * weights(i);
    hes_diag(i) = 2 * pow(delta, 2);
  }
  for (int i = 0; i < measured_supp_ids.size(); ++i) {
    int id = measured_supp_ids(i);
    phi(id) = 0;
    hes_diag(id) = 0;
  }
  FunctionTargetResult rslt;
  rslt.value = pow(phi, 2).sum();
  rslt.gradient = 2 * delta * phi;
  rslt.hessian = hes_diag.asDiagonal();
  return rslt;
}

FunctionTargetResult
cost_supp_default(VectorXd &x, VectorXd &vec_arg, MatrixXd &vec_mat,
                  std::vector<LinearModel> &linear_models) {
  FunctionTargetResult rslt;
  rslt.value = 0.0;
  rslt.gradient = vec_arg;
  rslt.hessian = vec_mat;
  return rslt;
}
