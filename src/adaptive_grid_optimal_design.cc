#include <Eigen/Dense>
#include <adaptive_grid_optimal_design.hh>
// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <ctime>
#include <limits>
using namespace Eigen;
// -- ADAPTIVE GRID OPTIMAL DESIGN ---------------------------------------------
// Method by
// * Duarte, Belmiro P., Weng Kee Wong und Holger Dette (2018).  Adaptive Grid
// * Semidefinite Programming for Finding Optimal Designs. In: Statistics and
// * Computing 28.2, S. 441–460. issn: 0960-3174.
// * doi: 10.1007/s11222-017-9741-y.
// * url: https://doi.org/10.1007/ s11222-017-9741-y

/** Filter Design
 * Delete design points with a weight <= filter_epsilon (e.g. 1e-5)
 */
void AdapativeGridOptimalDesign::filter_design(OptimalDesign &optimal_design) {
  logging::info("Filter Design");
  // count number of weights larger than epsilon
  int count = 0;
  std::vector<int> measured_supp_ids_vec;
  for (int i = 0; i < optimal_design.weights.size(); ++i) {
    if (optimal_design.weights(i) > option.filter_epsilon) {
      // count on measured ids in weights > filter epsilon
      if (design_option.measured_supp_set) {
        for (int j = 0; j < design_option.measured_supp_ids.size(); ++j) {
          if (i == design_option.measured_supp_ids(j)) {
            measured_supp_ids_vec.push_back(count);
          }
        }
      }
      count++;
    }
  }
  logging::info("Filter Design | before # supp = " +
                std::to_string(optimal_design.weights.size()) +
                ", after # supp = " + std::to_string(count));
  // filter weights
  VectorXd weights(count);
  MatrixXd supp(optimal_design.supp.rows(), count);
  int pos = 0;
  for (int i = 0; i < optimal_design.weights.size(); ++i) {
    if (optimal_design.weights(i) > option.filter_epsilon) {
      weights(pos) = optimal_design.weights(i);
      supp.col(pos) = optimal_design.supp.col(i);
      pos++;
    }
  }
  // update measured ids
  if (design_option.measured_supp_set) {
    design_option.measured_supp_ids = VectorXd(measured_supp_ids_vec.size());
    for (int i = 0; i < measured_supp_ids_vec.size(); ++i) {
      design_option.measured_supp_ids(i) = measured_supp_ids_vec[i];
    }
  }
  optimal_design.supp = supp;
  optimal_design.weights = weights / weights.lpNorm<1>();
  update_minimize_weights_option(option.weights.minimize, optimal_design.supp,
                                 optimal_design.weights);
}
/** Collapse Design
 * Collapse distance orientated cluster points to the point with the highest
 * weight
 */
void AdapativeGridOptimalDesign::collapse_design(
    OptimalDesign &optimal_design) {
  logging::info("Collapse design by clustering support points");
  struct Design_Point {
    VectorXd supp;
    double weight;
    int col_id;
  } design_point;
  // every cluster element contains a vector of design points
  std::vector<std::vector<Design_Point>> cluster;
  std::vector<Design_Point> cluster_point;
  Design_Point d_point;
  d_point.supp = optimal_design.supp.col(0);
  d_point.weight = optimal_design.weights(0);
  d_point.col_id = 0;
  cluster_point.push_back(d_point);
  cluster.push_back(cluster_point);
  // build cluster
  VectorXd point;
  VectorXd distance;
  bool append_to_cluster;
  int j;
  int k;
  for (int i = 1; i < optimal_design.supp.cols(); ++i) {
    point = optimal_design.supp.col(i);
    append_to_cluster = false;
    j = 0;
    while (j < cluster.size() && !append_to_cluster) {
      k = 0;
      while (k < cluster[j].size() && !append_to_cluster) {
        distance = cluster[j][k].supp - point;
        if (distance.norm() < option.collapse_epsilon) {
          d_point.supp = point;
          d_point.weight = optimal_design.weights(i);
          d_point.col_id = i;
          cluster[j].push_back(d_point);
          append_to_cluster = true;
        }
        k++;
      }
      j++;
    }
    if (!append_to_cluster) {
      cluster_point.clear();
      d_point.supp = point;
      d_point.weight = optimal_design.weights(i);
      d_point.col_id = i;
      cluster_point.push_back(d_point);
      cluster.push_back(cluster_point);
    }
  }
  logging::info("Collapse Design | Build " + std::to_string(cluster.size()) +
                " cluster with totally " +
                std::to_string(optimal_design.supp.cols()) +
                " support points.");
  // new number of support points
  int supp_size = cluster.size();
  MatrixXd supp(optimal_design.supp.rows(), supp_size);
  VectorXd weights(supp_size);
  std::vector<int> measured_supp_ids_vec;
  // reduce cluster to most weighted points
  int pos;
  double w;
  double w_best;
  double w_sum;
  for (int i = 0; i < cluster.size(); ++i) {
    // find most weighted points
    w_best = 0;
    w_sum = 0;
    pos = 0;
    for (int j = 0; j < cluster[i].size(); ++j) {
      w = cluster[i][j].weight;
      w_sum += w;
      if (w > w_best) {
        w_best = w;
        pos = j;
      }
    }
    supp.col(i) = cluster[i][pos].supp;
    weights(i) = w_sum;
    // is point in measured supp ids?
    if (design_option.measured_supp_set) {
      for (int n = 0; n < design_option.measured_supp_ids.size(); ++n) {
        if (cluster[i][pos].col_id == design_option.measured_supp_ids(n)) {
          measured_supp_ids_vec.push_back(i);
        }
      }
    }
  }
  if (design_option.measured_supp_set) {
    design_option.measured_supp_ids = VectorXd(measured_supp_ids_vec.size());
    for (int i = 0; i < measured_supp_ids_vec.size(); ++i) {
      design_option.measured_supp_ids(i) = measured_supp_ids_vec[i];
    }
  }
  logging::info("Collapse Design | before # supp = " +
                std::to_string(optimal_design.weights.size()) +
                ", after # supp = " + std::to_string(weights.size()));
  optimal_design.supp = supp;
  optimal_design.weights = weights;
}
/** Update minimize weights option
 * - minimze options for weight: bounds 0<=x<=1
 * - linear equality constraint matrix <=> sum(x) = 1
 * - xO: equal distributed or initialized
 */
void AdapativeGridOptimalDesign::update_minimize_weights_option(
    NLPMinimizeOption &min_opt, MatrixXd supp, VectorXd x0) {
  int supp_size = supp.cols();
  min_opt.x0 = x0;
  min_opt.bounds.x.u = VectorXd::Constant(supp_size, 1.0);
  min_opt.bounds.x.l = VectorXd::Constant(supp_size, 0.0);
  min_opt.lin_equal_constr.mat = MatrixXd::Constant(1, supp_size, 1.0);
}

void AdapativeGridOptimalDesign::trace_measure(OptimalDesign &design) {
  // push back trace measure: output measure
  option.trace.measures.push_back(to_string_with_precision(measure(design)));
}
// get measure from design xi
double AdapativeGridOptimalDesign::measure(OptimalDesign &design) {
  logging::info("Get measure of design for tracing option...");
  double measure;
  // d-criterium
  if ("d-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumWeights d_criterium(design.supp,
                                               design_option.linear_models[0]);
    // set measure value
    d_criterium.function.set_x(design.weights);
    // function val: - log det fisher
    double feat_size = (double)design_option.linear_models[0].feature_size();
    // real measure: det fisher
    measure = std::pow(std::exp(-d_criterium.function.val()), 1.0 / feat_size);
  } else if ("c-criterium" == design_option.design_type) {
    DesignMeasureCCriteriumWeights c_criterium(
        design_option.c_crit_m, design.supp, design_option.linear_models[0]);
    // set measure value
    c_criterium.function.set_x(design.weights);
    // function val: log(c^top M^-1 c)
    // real measure: c^top M^-1 c
    measure = std::exp(c_criterium.function.val());
  } else if ("cd-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumWeights d_criterium(design.supp,
                                               design_option.linear_models[0]);
    DesignMeasureCCriteriumWeights c_criterium(
        design_option.c_crit_m, design.supp, design_option.linear_models[1]);
    NLPFunction cd_criterium_function;
    cd_criterium_function.add_target(d_criterium.function_target,
                                     design_option.target_weights(0));
    cd_criterium_function.add_target(c_criterium.function_target,
                                     design_option.target_weights(1));
    // set measure value
    cd_criterium_function.set_x(design.weights);
    measure = cd_criterium_function.val();
  } else if ("odfs-criterium" == design_option.design_type) {
    // odfs
    DesignMeasureODFSWeights odfs_weight(
        design_option.linear_models, design.supp, design_option.c_crit_m,
        design_option.target_weights, design_option.cost_type,
        design_option.measured_supp_ids, design_option.cost_delta);
    // set measure value
    odfs_weight.function.set_x(design.weights);
    measure = odfs_weight.function.val();
  }
  return measure;
}

void AdapativeGridOptimalDesign::minimize_weights(
    OptimalDesign &optimal_design) {
  update_minimize_weights_option(option.weights.minimize, optimal_design.supp,
                                 optimal_design.weights);
  logging::info("Minimize weights start...");
  // d-criterium
  if ("d-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumWeights d_criterium(optimal_design.supp,
                                               design_option.linear_models[0]);
    NLPSolver solver(d_criterium.function, option.weights.solver);
    optimal_design.weights = solver.minimize(option.weights.minimize);
    filter_design(optimal_design);
    DesignMeasureDCriteriumWeights d_criterium_2(
        optimal_design.supp, design_option.linear_models[0]);
    NLPSolver solver_2(d_criterium_2.function, option.weights.solver);
    optimal_design.weights = solver_2.minimize(option.weights.minimize);
    // set measure value
    d_criterium_2.function.set_x(optimal_design.weights);
    optimal_design.measure = d_criterium_2.function.val();
  } else if ("c-criterium" == design_option.design_type) {
    DesignMeasureCCriteriumWeights c_criterium(design_option.c_crit_m,
                                               optimal_design.supp,
                                               design_option.linear_models[0]);
    NLPSolver solver(c_criterium.function, option.weights.solver);
    optimal_design.weights = solver.minimize(option.weights.minimize);
    filter_design(optimal_design);
    DesignMeasureCCriteriumWeights c_criterium_2(
        design_option.c_crit_m, optimal_design.supp,
        design_option.linear_models[0]);
    NLPSolver solver_2(c_criterium_2.function, option.weights.solver);
    optimal_design.weights = solver_2.minimize(option.weights.minimize);
    // set measure value
    c_criterium_2.function.set_x(optimal_design.weights);
    optimal_design.measure = c_criterium_2.function.val();
  } else if ("cd-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumWeights d_criterium(optimal_design.supp,
                                               design_option.linear_models[0]);
    DesignMeasureCCriteriumWeights c_criterium(design_option.c_crit_m,
                                               optimal_design.supp,
                                               design_option.linear_models[1]);
    NLPFunction cd_criterium_function;
    cd_criterium_function.add_target(d_criterium.function_target,
                                     design_option.target_weights(0));
    cd_criterium_function.add_target(c_criterium.function_target,
                                     design_option.target_weights(1));
    NLPSolver solver(cd_criterium_function, option.weights.solver);
    optimal_design.weights = solver.minimize(option.weights.minimize);
    filter_design(optimal_design);
    DesignMeasureDCriteriumWeights d_criterium_2(
        optimal_design.supp, design_option.linear_models[0]);
    DesignMeasureCCriteriumWeights c_criterium_2(
        design_option.c_crit_m, optimal_design.supp,
        design_option.linear_models[1]);
    NLPFunction cd_criterium_function_2;
    cd_criterium_function_2.add_target(d_criterium_2.function_target,
                                       design_option.target_weights(0));
    cd_criterium_function_2.add_target(c_criterium_2.function_target,
                                       design_option.target_weights(1));
    NLPSolver solver_2(cd_criterium_function_2, option.weights.solver);
    optimal_design.weights = solver_2.minimize(option.weights.minimize);
    // set measure value
    cd_criterium_function_2.set_x(optimal_design.weights);
    optimal_design.measure = cd_criterium_function_2.val();
  } else if ("odfs-criterium" == design_option.design_type) {
    // odfs
    DesignMeasureODFSWeights odfs_weight(
        design_option.linear_models, optimal_design.supp,
        design_option.c_crit_m, design_option.target_weights,
        design_option.cost_type, design_option.measured_supp_ids,
        design_option.cost_delta);
    NLPSolver solver(odfs_weight.function, option.weights.solver);
    optimal_design.weights = solver.minimize(option.weights.minimize);
    filter_design(optimal_design);
    DesignMeasureODFSWeights odfs_weight_2(
        design_option.linear_models, optimal_design.supp,
        design_option.c_crit_m, design_option.target_weights,
        design_option.cost_type, design_option.measured_supp_ids,
        design_option.cost_delta);
    NLPSolver solver_2(odfs_weight_2.function, option.weights.solver);
    optimal_design.weights = solver_2.minimize(option.weights.minimize);
    // set measure value
    odfs_weight_2.function.set_x(optimal_design.weights);
    optimal_design.measure = odfs_weight_2.function.val();
    // std::cout << "Measure: " << optimal_design.measure << '\n';
  }
  logging::info("Minimize weights finished.");
}

void AdapativeGridOptimalDesign::minimize_supp(OptimalDesign &optimal_design) {
  logging::info("Minimize supp start...");
  // d-criterium
  if ("d-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumSupp d_criterium_supp(
        optimal_design.weights, optimal_design.supp,
        design_option.linear_models[0]);
    NLPSolver solver_supp(d_criterium_supp.function, option.supp.solver);
    option.supp.minimize.inequal_constr.use_template = "voronoi";
    option.supp.minimize.inequal_constr.voronoi_supp = optimal_design.supp;
    for (int x_id = 0; x_id < optimal_design.supp.cols(); ++x_id) {
      option.supp.minimize.inequal_constr.voronoi_x_id = x_id;
      option.supp.minimize.x0 = optimal_design.supp.col(x_id);
      optimal_design.supp.col(x_id) =
          solver_supp.minimize(option.supp.minimize);
    }
  } else if ("c-criterium" == design_option.design_type) {
    DesignMeasureCCriteriumSupp c_criterium_supp(
        design_option.c_crit_m, optimal_design.weights, optimal_design.supp,
        design_option.linear_models[0]);
    NLPSolver solver_supp(c_criterium_supp.function, option.supp.solver);
    option.supp.minimize.inequal_constr.use_template = "voronoi";
    option.supp.minimize.inequal_constr.voronoi_supp = optimal_design.supp;
    for (int x_id = 0; x_id < optimal_design.supp.cols(); ++x_id) {
      option.supp.minimize.inequal_constr.voronoi_x_id = x_id;
      option.supp.minimize.x0 = optimal_design.supp.col(x_id);
      optimal_design.supp.col(x_id) =
          solver_supp.minimize(option.supp.minimize);
    }
  } else if ("cd-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumSupp d_criterium_supp(
        optimal_design.weights, optimal_design.supp,
        design_option.linear_models[0]);
    DesignMeasureCCriteriumSupp c_criterium_supp(
        design_option.c_crit_m, optimal_design.weights, optimal_design.supp,
        design_option.linear_models[1]);
    NLPFunction cd_criterium_function;
    cd_criterium_function.add_target(d_criterium_supp.function_target,
                                     design_option.target_weights(0));
    cd_criterium_function.add_target(c_criterium_supp.function_target,
                                     design_option.target_weights(1));
    NLPSolver solver_supp(cd_criterium_function, option.supp.solver);
    option.supp.minimize.inequal_constr.use_template = "voronoi";
    option.supp.minimize.inequal_constr.voronoi_supp = optimal_design.supp;
    for (int x_id = 0; x_id < optimal_design.supp.cols(); ++x_id) {
      option.supp.minimize.inequal_constr.voronoi_x_id = x_id;
      option.supp.minimize.x0 = optimal_design.supp.col(x_id);
      optimal_design.supp.col(x_id) =
          solver_supp.minimize(option.supp.minimize);
    }
  } else if ("odfs-criterium" == design_option.design_type) {
    DesignMeasureODFSSupp odfs_supp(
        design_option.linear_models, optimal_design.weights,
        optimal_design.supp, design_option.c_crit_m,
        design_option.target_weights, design_option.cost_type,
        design_option.measured_supp_ids, design_option.cost_delta);
    NLPSolver solver_supp(odfs_supp.function, option.supp.solver);
    option.supp.minimize.inequal_constr.use_template = "voronoi";
    option.supp.minimize.inequal_constr.voronoi_supp = optimal_design.supp;
    bool x_is_measured;
    for (int x_id = 0; x_id < optimal_design.supp.cols(); ++x_id) {
      x_is_measured = false;
      for (int i = 0; i < design_option.measured_supp_ids.size(); ++i) {
        if (x_id == design_option.measured_supp_ids(i)) {
          x_is_measured = true;
        }
      }
      if (!x_is_measured) {
        option.supp.minimize.inequal_constr.voronoi_x_id = x_id;
        option.supp.minimize.x0 = optimal_design.supp.col(x_id);
        optimal_design.supp.col(x_id) =
            solver_supp.minimize(option.supp.minimize);
      }
    }
  }
  logging::info("Minimize supp finished.");
}

/** Adaptive Grid Optimal Design Solver
 */
OptimalDesign AdapativeGridOptimalDesign::solve(MatrixXd supp,
                                                bool shuffle_option) {
  logging::info("Adaptive grid optimal design | solve");
  OptimalDesign optimal_design;
  if (design_option.measured_supp_ids.size() > 0) {
    design_option.measured_supp_set = true;
  }
  if (shuffle_option) {
    logging::info("AGOD | Shuffle supp");
    // shuffle support points and measured supp ids equivalent
    int size = supp.cols();
    PermutationMatrix<Dynamic, Dynamic> perm(size);
    perm.setIdentity();
    std::srand(std::time(nullptr));
    std::random_shuffle(perm.indices().data(),
                        perm.indices().data() + perm.indices().size());
    optimal_design.supp = supp * perm;
    if (design_option.measured_supp_set) {
      for (int i = 0; i < design_option.measured_supp_ids.size(); ++i) {
        int old_col = design_option.measured_supp_ids(i);
        for (int j = 0; j < size; ++j) {
          if (old_col == perm.indices()(j)) {
            // old col switch to new col j
            design_option.measured_supp_ids(i) = j;
          }
        }
      }
    }
  } else {
    optimal_design.supp = supp;
  }
  // init weights unique distributed
  optimal_design.weights = VectorXd::Constant(supp.cols(), 1.0 / supp.cols());
  // minimize for weights
  int iter = 0;
  // trace options: weights, dispersion, measure
  if (option.trace.weights) {
    export_weights(optimal_design, iter - 1);
  }
  if (option.trace.measure) {
    trace_measure(optimal_design);
  }
  // minimize via weights at 'iteration 0'
  minimize_weights(optimal_design);

  if (option.trace.weights) {
    export_weights(optimal_design, iter);
  }
  if (option.trace.measure) {
    trace_measure(optimal_design);
  }
  // begin loop: minimize supp and weights
  MatrixXd distance_supp;
  double distance;
  bool distance_proof = true;
  bool measure_proof = true;
  while (iter++ < option.max_iter && distance_proof && measure_proof) {
    logging::info("AGOD | Iteration: " + std::to_string(iter));
    auto old_optimal_design = optimal_design;
    // minimize criterium for all support points
    MatrixXd old_supp = optimal_design.supp;
    minimize_supp(optimal_design);
    distance_supp = old_supp - optimal_design.supp;
    distance = distance_supp.norm();
    // collapse updated design
    collapse_design(optimal_design);
    // minimize weights
    minimize_weights(optimal_design);
    // trace options
    if (optimal_design.measure > old_optimal_design.measure ||
        std::abs(optimal_design.measure - old_optimal_design.measure) <
            option.measure_precision * old_optimal_design.measure) {
      measure_proof = false;
      optimal_design = old_optimal_design;
    } else if (distance < optimal_design.supp.norm() * option.supp_precision) {
      distance_proof = false;
    } else {
      if (option.trace.weights) {
        export_weights(optimal_design, iter);
      }
      if (option.trace.measure) {
        trace_measure(optimal_design);
      }
      // export dispersion trace
      if (option.trace.dispersion) {
        export_dispersion(old_optimal_design, iter);
      }
    }
  }
  // export trace measures
  if (option.trace.measure) {
    export_measure();
  }
  // logging termination reason
  if (!(iter++ < option.max_iter)) {
    logging::info("AGOD | Termination reason: Max. iterations reached, " +
                  std::to_string(iter) + " iterations");
  } else if (!distance_proof) {
    logging::info("AGOD | Termination reason: supp distance " +
                  to_string_with_precision(distance));
  } else if (!measure_proof) {
    logging::info("AGOD | Termination reason: local minimum reached ");
  }
  // set correct measure of optimal design
  optimal_design.measure = measure(optimal_design);
  return optimal_design;
}

void AdapativeGridOptimalDesign::logging_options() {
  logging::info("AGOD:Design-Options");
  logging::info("design_type = " + design_option.design_type);
  logging::info("cost_type = " + design_option.cost_type);
  logging::info("cost_delta = " + std::to_string(design_option.cost_delta));
  logging::info("linear_models size = " +
                std::to_string(design_option.linear_models.size()));

  logging::info("AGOD:General-Options");
  logging::info("filter_epsilon = " +
                to_string_with_precision(option.filter_epsilon));
  logging::info("collapse_epsilon = " +
                to_string_with_precision(option.collapse_epsilon));
  logging::info("supp_precision = " +
                to_string_with_precision(option.supp_precision));
  logging::info("max_iter = " + std::to_string(option.max_iter));
}
// export weights
void AdapativeGridOptimalDesign::export_weights(OptimalDesign &optimal_design,
                                                int iter) {
  logging::info("Export weights...");
  int supp_dim = optimal_design.supp.col(0).size();
  std::vector<std::string> table_weights_title{"x_0"};
  for (int i = 1; i < supp_dim; ++i) {
    table_weights_title.push_back("x_" + std::to_string(i));
  }
  table_weights_title.push_back("w");
  // write string
  std::vector<std::string> table_weights = table_weights_title;
  for (int i = 0; i < optimal_design.weights.size(); ++i) {
    for (int m = 0; m < supp_dim; ++m) {
      table_weights.push_back(
          to_string_with_precision(optimal_design.supp.col(i)(m)));
    }
    table_weights.push_back(
        to_string_with_precision(optimal_design.weights(i)));
  }
  export_table(option.simulation_id, option.folder_id,
               "agod_weights_iter_" + std::to_string(iter), table_weights,
               supp_dim + 1);
}

// export weights
void AdapativeGridOptimalDesign::export_measure() {
  logging::info("Export measure...");
  std::vector<std::string> measures_table{"iteration"};
  measures_table.push_back("measure");
  for (int i = 0; i < option.trace.measures.size(); ++i) {
    measures_table.push_back(std::to_string(i - 1));
    measures_table.push_back(option.trace.measures[i]);
  }
  export_table(option.simulation_id, option.folder_id, "crit_measures",
               measures_table, 2);
}

// export dispersion
void AdapativeGridOptimalDesign::export_dispersion(OptimalDesign &design,
                                                   int iter) {
  logging::info("Export dispersion values on design space...");
  // dispersion function
  NLPFunction dispersion_function;
  // d-criterium
  if ("d-criterium" == design_option.design_type) {
    DesignMeasureDCriteriumSupp d_criterium_supp(
        design.weights, design.supp, design_option.linear_models[0]);
    dispersion_function = d_criterium_supp.function;
  }
  // compute sampling dispersion values
  int dim_x = design.supp.rows();
  // define design space points
  VectorXd x_sampling =
      VectorXd::Constant(dim_x, option.trace.dispersion_sampling_per_dim);

  DesignMeasureGrid x_grid_build(option.supp.minimize.bounds.x.l,
                                 option.supp.minimize.bounds.x.u, x_sampling);
  MatrixXd x_grid = x_grid_build.supp;
  std::vector<std::string> dispersion_table{"x_0"};
  for (int i = 1; i < dim_x; ++i) {
    dispersion_table.push_back("x_" + std::to_string(i));
  }
  dispersion_table.push_back("dispersion");
  for (int i = 0; i < x_grid.cols(); ++i) {
    // x_i values
    VectorXd x_point = x_grid.col(i);
    for (int j = 0; j < x_grid.rows(); ++j) {
      dispersion_table.push_back(to_string_with_precision(x_point(j)));
    }
    // dispersion value
    dispersion_function.set_x(x_point);
    dispersion_table.push_back(
        to_string_with_precision(-dispersion_function.val()));
  }
  export_table(option.simulation_id, option.folder_id,
               "agod_dispersion_iter_" + std::to_string(iter), dispersion_table,
               dim_x + 1);
}
